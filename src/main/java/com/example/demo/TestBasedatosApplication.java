package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestBasedatosApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestBasedatosApplication.class, args);
	}

}
